// https://github.com/sbt/sbt-dynver
addSbtPlugin("com.dwijnand" % "sbt-dynver" % "4.1.1")

// https://github.com/xerial/sbt-sonatype
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "3.9.15")
// https://github.com/sbt/sbt-pgp
addSbtPlugin("com.github.sbt" % "sbt-pgp" % "2.2.1")
